<?php

declare(strict_types=1);

namespace EmailQueue\Mailer;

use Cake\I18n\I18n;
use Cake\Mailer\Mailer;
use Cake\Routing\Router;
use EmailQueue\EmailQueue;
use Pelago\Emogrifier\CssInliner;
use Pelago\Emogrifier\HtmlProcessor\HtmlPruner;
use Cake\Mailer\Exception\MissingActionException;
use Pelago\Emogrifier\HtmlProcessor\CssToAttributeConverter;

/**
 * Letters mailer.
 */
class QueueMailer extends Mailer
{
    private $currentLocale = false;

    private $configuration = 'default';


    public function setLocale($locale)
    {
        if (!$this->currentLocale) {
            $this->currentLocale = I18n::getLocale();
        }

        I18n::setlocale($locale);
        Router::reload();
    }

    public function setConfiguration($configuration)
    {
        $this->configuration = $configuration;
    }

    public function deliver(string $content = ''): array
    {
        $this->render($content);
        $content = $this->message->getBodyHtml();
        // $content = $this->emogrifier($content);
        $options = [
            'subject' => $this->message->getOriginalSubject(),
            'from_name' => current($this->message->getFrom()),
            'from_email' => key($this->message->getFrom()),
            'format' => 'html',
            'layout' => 'EmailQueue.queue',
            'template' => 'EmailQueue.queue',
            'config' => $this->configuration,
            'attachments' => $this->message->getAttachments(),
            'cc' => implode(',', $this->message->getCc()),
            'bcc' => implode(',', $this->message->getBcc()),
        ];

        EmailQueue::enqueue($this->message->getTo(), ['body' => $content], $options);
        return [];
    }

    /**
     * Embebe los estilos CSS del fichero /css/email.css dentro del mail
     *
     * @param string $content
     * @return string
     */
    public function emogrifier($content)
    {
        $file_css = ROOT . DS . 'webroot' . DS . 'css' . DS . 'email.css';

        if (!file_exists($file_css)) {
            return $content;
        }

        $css = file_get_contents($file_css);
        $css = str_replace('@charset "UTF-8";', '', $css);
        $cssInliner = CssInliner::fromHtml($content)->inlineCss($css);
        $domDocument = $cssInliner->getDomDocument();
        HtmlPruner::fromDomDocument($domDocument)->removeElementsWithDisplayNone()
            ->removeRedundantClassesAfterCssInlined($cssInliner);
        $mergedHtml = CssToAttributeConverter::fromDomDocument($domDocument)
            ->convertCssToVisualAttributes()->render();

        return $mergedHtml;
    }
}
